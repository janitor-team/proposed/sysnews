.\" Copyright (C) 1993,1994,1995  Charles (int@link.xs4all.nl)
.\"
.\" GPL
.\"
.\" $Id: news.man,v 1.4 1997/01/08 10:09:37 root Exp $
.\"
.TH NEWS 1 "18 January 1995" "Linux" "USER COMMANDS"
.SH NAME
news \- display system news
.SH SYNOPSIS
.B news [\-adDeflnpvxs] [[article1] [article2] ..]
.SH DESCRIPTION
The
.B news
command keeps you informed of news concerning the system.
Each news item is contained in a separate file in the
.I /var/sysnews
directory. Anyone having write permission
to this directory can create a news file.
.LP
If you run the news command without any flags, it displays every
unread file in the
.I /var/sysnews
directory.
.LP
Each file is preceded by an appropriate header. To avoid reporting
old news, the news command stores a currency time. The news
command considers your currency time to be the date the
.I $HOME/.news_time
file was last modified. Each time you read the
news, the modification time of this file changes to that of the
reading. Only news item files posted after this time are considered
unread.
.LP
.SH OPTIONS
.TP
.I "\-a, \-\-all"
Display all news, also the already read news.
.TP
.I \-d, \-\-datestamp
Add a date stamp to each article name printed. this can only be used with
the \-nl flags.
.TP
.I "\-D, \-\-datefmt <fmt>"
Specify a date format, see the
.BR strftime (3)
man page for more details.
the default format is
.BR "(%b %d %Y)"
.TP
.I "\-f, \-\-newsdir <dir>"
Read news from an alternate newsdir.
.TP
.I "\-l, \-\-oneperline"
One article name per line.
.TP
.I "\-n, \-\-names"
Only show the names of news articles.
.TP
.I "\-p, \-\-page"
Pipe articles through
.B $PAGER
or
.BR more (1)
if the
.B $PAGER
environment variable is not set.
.TP
.I "\-s, \-\-articles"
Reports the number of news articles.
.LP
.SH MAINTAINER OPTIONS
.TP
.I "\-e, \-\-expire #"
Expire news older than # days.
.TP
.I "\-x, \-\-exclude a,b,c"
A comma separated list of articles which may not be expired.
if a file named
.I .noexpire
exists in the
.I /var/sysnews
direcory, filenames are read from it also. names in this file may be comma
separated, and/or one per line.
.SH AUTHOR
Charles, <int@link.xs4all.nl>
